package com.evil.ifinance;

import com.evil.ifinance.util.HibernateUtil;
import org.hibernate.Session;

public class IFinanceApp {
    
    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.close();
    }
    
}
