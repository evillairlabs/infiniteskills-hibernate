package com.evil.ifinance.util;

import com.evil.ifinance.data.entity.User;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {
            // holds all Hibernate specific properties & mapping information
            Configuration configuration = new Configuration();
            // following lines if getting from properties file
            configuration.addAnnotatedClass(User.class);
            return configuration.buildSessionFactory(new StandardServiceRegistryBuilder().build());
            
            // following lines if getting from xml file
//            return configuration.buildSessionFactory(new StandardServiceRegistryBuilder()
//                    .applySettings( configuration.getProperties() )
//                    .build()
//            );
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error building the factory");
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

}